package com.epicodus.madlibs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class StoryActivity extends AppCompatActivity {
    private TextView mStoryTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);
        mStoryTextView = (TextView) findViewById(R.id.storyTextView);
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String place = intent.getStringExtra("place");
        String animal = intent.getStringExtra("animal");
        String emotion = intent.getStringExtra("emotion");
        String dessert = intent.getStringExtra("dessert");
        String desert = intent.getStringExtra("desert");
        String adjective = intent.getStringExtra("adjective");
        String organ = intent.getStringExtra("organ");
        mStoryTextView.setText("There once was a teapot called " + name + ". It was made in " + place + " but it spent most of the last 50 years in an attic. One time a " + animal + " tried to run away with " + name + " but Amanda, full of " + emotion + " lured the " + animal + " away with some " + dessert + ". \"I think I'll take this teapot to the " + desert + " It's really " + adjective + " there and I hear the dried " + organ + " pancakes are very odd.");
    }
}
