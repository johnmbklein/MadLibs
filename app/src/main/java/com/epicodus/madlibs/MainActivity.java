package com.epicodus.madlibs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();
    private Button mMakeStoryButton;
    private EditText mNameInput;
    private EditText mPlaceInput;
    private EditText mAnimalInput;
    private EditText mEmotionInput;
    private EditText mDessertInput;
    private EditText mDesertInput;
    private EditText mAdjectiveInput;
    private EditText mOrganInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNameInput = (EditText) findViewById(R.id.nameInput);
        mPlaceInput = (EditText) findViewById(R.id.placeInput);
        mAnimalInput = (EditText) findViewById(R.id.animalInput);
        mEmotionInput = (EditText) findViewById(R.id.emotionInput);
        mDessertInput = (EditText) findViewById(R.id.dessertInput);
        mDesertInput = (EditText) findViewById(R.id.desertInput);
        mAdjectiveInput = (EditText) findViewById(R.id.adjectiveInput);
        mOrganInput = (EditText) findViewById(R.id.organInput);

        mMakeStoryButton = (Button) findViewById(R.id.makeStoryButton);

        mMakeStoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mNameInput.getText().toString();
                String place = mPlaceInput.getText().toString();
                String animal = mAnimalInput.getText().toString();
                String emotion = mEmotionInput.getText().toString();
                String dessert = mDessertInput.getText().toString();
                String desert = mDesertInput.getText().toString();
                String adjective = mAdjectiveInput.getText().toString();
                String organ = mOrganInput.getText().toString();
                Intent intent = new Intent(MainActivity.this, StoryActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("place", place);
                intent.putExtra("animal", animal);
                intent.putExtra("emotion", emotion);
                intent.putExtra("dessert", dessert);
                intent.putExtra("desert", desert);
                intent.putExtra("adjective", adjective);
                intent.putExtra("organ", organ);

                startActivity(intent);
            }
        });
    }
}
